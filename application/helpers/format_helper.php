<?php

function format_rupiah($rph)
{
    return "Rp. " . number_format($rph) . ",00";
}

function format_date($date)
{
    $bulan = array(
        'januari',
        'februari',
        'maret',
        'april',
        'mei',
        'juni',
        'juli',
        'agustus',
        'september',
        'oktober',
        'november',
        'desember',
    );
    $divide = explode("-", $date);
    return $divide[2].' '.$bulan[(int) $divide[1]].' '.$divide[0];
}
