<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('memory_limit', '2048M');
set_time_limit(5000);
class Import extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('model_qrcode');
    }

    public function index()
    {
        $this->load->view('v_import');
    }

    public function upload()
    {
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        $config['upload_path'] = realpath('excel');
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {

            //upload gagal
            $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES IMPORT GAGAL!</b> '.$this->upload->display_errors().'</div>');
            //redirect halaman
            redirect(base_url('admin/masteru'), 'refresh');

        } else {

            $data_upload = $this->upload->data();

            $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('excel/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

            // $data = array();

            $numrow = 1;
            foreach($sheet as $row){
                if($numrow > 1){

                    $kode = $row['C'].'-'.$row['D'];
                    // $nup = $row['E'];

                    $this->load->library('ciqrcode'); //pemanggilan library QR CODE

                    $config['cacheable']    = true; //boolean, the default is true
                    $config['cachedir']     = './assets/'; //string, the default is application/cache/
                    $config['errorlog']     = './assets/'; //string, the default is application/logs/
                    $config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
                    $config['quality']      = true; //boolean, the default is true
                    $config['size']         = '1024'; //interger, the default is 1024
                    $config['black']        = array(224,255,255); // array, default is array(255,255,255)
                    $config['white']        = array(70,130,180); // array, default is array(0,0,0)
                    $this->ciqrcode->initialize($config);

                    $image_name= $kode.'.png'; //buat name dari qr code sesuai dengan kd_barang

                    // $params['data'] = $kd_barang.'-'.$nup; //data yang akan di jadikan QR CODE
                    $params['data'] = $kode;
                    // $params['data'] = $nup; //data yang akan di jadikan QR CODE
                    $params['level'] = 'H'; //H=High
                    $params['size'] = 10;
                    $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
                    $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

                    $data = array(
                        'kode'          => $kode,
                        'kd_lokasi'     => $row['B'],
                        'kd_brg'        => $row['C'],
                        'no_aset'       => $row['D'],
                        'ur_baru'       => $row['E'],
                        'tgl_perlh'     => $row['F'],
                        'merk_type'     => $row['G'],
                        'asal_perlh'    => $row['H'],
                        'rph_sat'       => $row['I'],
                        'keterangan'    => $row['J'],
                        'no_mesin'      => $row['K'],
                        'no_rangka'     => $row['L'],
                        'no_polisi'     => $row['M'],
                        'no_bpkb'       => $row['N'],
                        'qr_code'       => $image_name
                    );

                    $insert_data = $this->model_qrcode->tambah($data);
                }
                $numrow++;
            }
            // echo '<pre>';
            // print_r($data);die;
            // $this->db->insert_batch('report', $data);
            //delete file from server
            unlink(realpath('excel/'.$data_upload['file_name']));

            //upload success
            $this->session->set_flashdata('notif', '<div class="alert alert-success"><b>PROSES IMPORT BERHASIL!</b> Data berhasil diimport!</div>');
            //redirect halaman
            redirect(base_url('admin/masteru'), 'refresh');

        }
    }

}
