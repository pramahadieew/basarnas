<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Berita_model');
        $this->load->model('guestModel');
    }
    public function index()
    {
        $berita = $this->Berita_model->listing();
        $params = [
            'title'   => 'Home',
            'content' => 'home',
            // 'items'  => $berita
        ];
        $this->load->view('frontend/welcome_message', ['layout' => $params, 'items' => $berita]);
    }
    public function tambah()
    {
        $data = [
            'nama'     => $this->input->post('nama'),
            'email'    => $this->input->post('email'),
            'instansi' => $this->input->post('instansi'),
            'notelp'   => $this->input->post('telp'),
            'dibuat'   => strtotime("now"),
        ];

        $this->guestModel->insert($data);
        return header("Location:" . base_url());

    }
}
