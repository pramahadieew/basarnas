<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dummy extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function index()
    {
        $this->load->view('dummy.php');
    }

    public function insert()
    {
        // jumlah data yang akan di insert
        $jumlah_data = 600000;
        for ($i = 1; $i <= $jumlah_data; $i++) {
            $data = array(
                "id" => $i,
                "kd_satker" => "85395837" . $i,
                "kd_barang" => "4982842" . $i,
                "nama_barang" => 'barang' . $i,
                "nup" => "23" . $i,
                "merek" => 'sama',
                "nama_penyedia" => 'dia',
                "nilai_barang" => '45',
                "keterangan_barang" => 'sama aja',
                "lokasi" => 'disini sama');
            $this->db->insert('report', $data);
        }
        echo $i . ' Data Berhasil Di Insert';
    }

    public function json($kd_brg='', $startNup='', $endNup='')
    {
        $this->load->library('datatables');

        $this->datatables->select('id, kd_lokasi, kd_brg, no_aset, ur_baru, tgl_perlh, merk_type, asal_perlh, rph_sat, keterangan, no_mesin, no_rangka, no_polisi, no_bpkb');
        $this->datatables->from('report');
        $this->datatables->add_column(
            'view',
            '
                <a href="'.site_url().'admin/Masteru/edit/$1" onclick="return confirm(' . "'Are You Sure Want To Edit Asset : Kode Barang " . '$3' . " ???'" . ')"><button class="btn btn-success btn-xs" title="Edit Data Asset"><i class="fa fa-edit"></i></button></a>
                <a href="'.site_url().'admin/Masteru/delete/$1" onclick="return confirm(' . "'Are You Sure Want To Delete Asset : Kode Barang " . '$3' . " ???'" . ')"><button class="btn btn-danger btn-xs" title="Hapus Data Asset"><i class="fa fa-trash"></i></button></a>
            ',
            'id, kd_lokasi, kd_brg, no_aset, ur_baru, tgl_perlh, merk_type, asal_perlh, rph_sat, keterangan, no_mesin, no_rangka, no_polisi, no_bpkb'
        );

        if ($startNup != '-' && $endNup != '-' && $endNup >= $startNup) {
            $this->datatables->where(array('no_aset>=' => $startNup));
            $this->datatables->where(array('no_aset<=' => $endNup));

            if($kd_brg!='-')
            {
                $this->datatables->where(array('kd_brg' => $kd_brg));
            }
            
        } elseif ($kd_brg != '-') {
            $this->datatables->where(array('kd_brg' => $kd_brg));
        }

        $data = $this->datatables->generate();

        header('Content-Type: application/json');
        echo $data;
    }

    // public function select_filter($thnpelaksanaan, $idkejuruan, $tahap)
    // {
    //     $this->datatables->select('id_annual_schedule, jenis_kelas, thnpelaksanaan, namaprogram, cevest_transpaket.keterangan, tglpelaksanaan, tglselesai, jumlah_jp, is_publish, totaljp, status_sesi');
    //     $this->datatables->from('abseva_annual_schedule');
    //     $this->datatables->join('cevest_transpaket', 'transpaket=cevest_transpaket.idtrans');
    //     $this->datatables->join('cevest_progpel', 'cevest_transpaket.idprog=cevest_progpel.idprog');
    //     $this->datatables->join('cevest_subkejuruan', 'cevest_progpel.idsubjur=cevest_subkejuruan.idsubjur');
    //     $this->datatables->join('cevest_kejuruan', 'cevest_subkejuruan.idkejuruan=cevest_kejuruan.idkejuruan');
    //     $this->datatables->add_column(
    //         'view',
    //         '
    //             <a href="'.base_url().'penjadwalan/Annual_schedule/approve/$1" onclick="return confirm(' . "'Are You Sure Want To Approve and Publish This Annual Schedule : Kelas " . '$2' . " , Program " . '$3' . " - " . '$4' . " , Jumlah JP " . '$8' . " ???'" . ')"><button class="btn btn-success btn-xs" title="Approve and Publish Data Annual Schedule"><i class="fa fa-check-square-o"></i></button></a>
    //             <a href="'.base_url().'penjadwalan/Annual_schedule/annual_schedule_excel/$1" onclick="return confirm(' . "'Are You Sure Want To Export Excel Annual Schedule : Kelas " . '$2' . " , Program " . '$3' . " - " . '$4' . " , Jumlah JP " . '$8' . " ???'" . ')"><button class="btn btn-success btn-xs" title="Export Excel Data Annual Schedule"><i class="fa fa-file-excel-o"></i></button></a>
    //             <a href="'.base_url().'penjadwalan/Annual_schedule/detail/$1" onclick="return confirm(' . "'Are You Sure Want To Show Detail Annual Schedule : Kelas " . '$2' . " , Program " . '$3' . " - " . '$4' . " , Jumlah JP " . '$8' . " ???'" . ')"><button class="btn btn-primary btn-xs" title="Show Data Detail Annual Schedule"><i class="fa fa-calendar"></i></button></a>
    //             <a href="'.base_url().'penjadwalan/Annual_schedule/edit/$1" onclick="return confirm(' . "'Are You Sure Want To Edit Annual Schedule : Kelas " . '$2' . " , Program " . '$3' . " - " . '$4' . " , Jumlah JP " . '$8' . " ???'" . ')"><button class="btn btn-success btn-xs" title="Edit Data Annual Schedule"><i class="fa fa-edit"></i></button></a>
    //             <a href="'.base_url().'penjadwalan/Annual_schedule/action_delete/$1" onclick="return confirm(' . "'Are You Sure Want To Delete Annual Schedule : Kelas " . '$2' . " , Program " . '$3' . " - " . '$4' . " , Jumlah JP " . '$8' . " ???'" . ')"><button class="btn btn-danger btn-xs" title="Hapus Data Annual Schedule"><i class="fa fa-trash"></i></button></a>
    //         ',
    //         'id_annual_schedule, jenis_kelas, thnpelaksanaan, namaprogram, cevest_transpaket.keterangan, tglpelaksanaan, tglselesai, jumlah_jp, is_publish, totaljp, status_sesi'
    //     );

    //     if($thnpelaksanaan!='-')
    //     {
    //         $this->datatables->where(array('cevest_transpaket.thnpelaksanaan' => $thnpelaksanaan)); 
    //     }
    //     if($idkejuruan!='-')
    //     {
    //         $this->datatables->where(array('cevest_kejuruan.idkejuruan' => $idkejuruan));   
    //     }
    //     if($tahap!='-')
    //     {
    //         $this->datatables->where(array('cevest_transpaket.keterangan' => $tahap));  
    //     }

    //     if($this->session->userdata('log_sess_nama_role')=='Kejuruan')
    //     {
    //         $this->datatables->where(array('cevest_kejuruan.idkejuruan' => $this->session->userdata('log_sess_kejuruan'))); 
    //     }

    //     $this->db->order_by('id_annual_schedule', 'ASC');
    //     return $this->datatables->generate();
    // }

    // public function get_data_json($thnpelaksanaan='', $idkejuruan='', $tahap='') 
    // {
    //     if($thnpelaksanaan || $idkejuruan || $tahap)
    //     {
    //         header('Content-Type: application/json');
    //         echo $this->MAnnual_schedule->select_filter($thnpelaksanaan,$idkejuruan,$tahap);
    //     }
    //     else
    //     {
    //         header('Content-Type: application/json');
    //         echo $this->MAnnual_schedule->select();
    //     }
    // }

}
