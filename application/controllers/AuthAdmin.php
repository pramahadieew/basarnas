<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AuthAdmin extends CI_Controller
{

    //load Login_model
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }
    //halaman Login
    //akan mengecek beberapa variable
    public function index()
    {
        $valid = $this->form_validation;

        $valid->set_rules('username', 'Username', 'required',
            array('required' => 'Username harus diisi'));

        $valid->set_rules('password', 'Password', 'required|min_length[6]',
            array('required' => 'Password harus diisi',
                'min_length' => 'Password Minimal 6 Karakter'));

        if ($valid->run() === false) {
            //End Validasi
            $data = array('title' => 'SAR Aplikasi Labelisasi Aset');
            $this->load->view('admin/auth/login', $data, false);

            //Validasi Usename dan Password
            //Check nama dan Password compare dengan database
        } else {
            $i = $this->input;
            $username = $i->post('username');
            $password = sha1($i->post('password'));

            //Check di database
            $check_login = $this->user_model->login($username, $password);
            // print_r($check_login);

            //Jika Ada Record pada Database, maka create Session dan Redirect ke halaman Dasbor
            if ($check_login != null) {
                $this->session->set_userdata('username', $username);
                $this->session->set_userdata('level', $check_login->level);
                $this->session->set_userdata('nama_id', $check_login->nama_id);
                // $this->session->set_userdata('status', $check_login->status);
                redirect(base_url('admin/dashboard'), 'refresh');

            } else {
                //kalau nama dan passord tidak cocok, Erorr
                $this->session->set_flashdata('gagal', 'Username dan Password Tidak Cocok / Salah');
                redirect(base_url('AuthAdmin'), 'refresh');

            }
        }
    }
    //Logout Session
    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('level');
        $this->session->unset_userdata('nama_id');
        // $this->session->unset_userdata('status');
        $this->session->set_flashdata('sukses', 'Anda Berhasil Logout');
        redirect(base_url('AuthAdmin'), 'refresh');

    }

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
