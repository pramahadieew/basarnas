<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Scan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_scan');
    }

    public function index()
    {
        $scan = $this->input->get('scan', true);
        if ($scan) {
            $scan = explode('-', $scan);
            if (count($scan) == 2) {
                $param = array('kd_brg' => $scan[0], 'no_aset' => $scan[1]);
                $qrcode = $this->model_scan->listing($param);
                if (!$qrcode) {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger">Tidak ada data</div>');
                    redirect(base_url('admin/scan'));
                }
            } else {
                // print_r('kode input tidak sesuai');die;
                $this->session->set_flashdata('notif', '<div class="alert alert-warning">Kode Tidak Sesuai</div>');
                redirect(base_url('admin/scan'));
            }
        } else {
            $qrcode = '';
        }

        $data = array(
            'title' => 'Scan Data Aset',
            'items' => $qrcode,
            'isi' => 'admin/scan/list',
        );
        $this->load->view('admin/layout/wrapper', $data);
    }
}

/* End of file user.php */
