<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Dashboard extends CI_Controller {

	//load Model

	public function __construct()

	{

		parent::__construct();

		$this->load->model('user_model');

	}



	public function index()

	{

		$data = array( 'title'   => 'Welcome To Aplikasi Labelisasi Aset',

		               'isi'     => 'admin/dasbor/list' );

		$this->load->view('admin/layout/wrapper', $data);	

	}

	public function profile() {

		$id_user = $this->session->userdata('id_user');

		$user = $this->user_model->detail($id_user);



		//Validasi

		$valid=$this->form_validation;



		$valid->set_rules('nama','Nama','required', 

			array(	'required'		=>	'Nama harus diisi'));



		$valid->set_rules('email','Email','required|valid_email', 

			array(	'required'		=>	'Email harus diisi',

					'valid_email'	=>	'Format Email tidak benar',

					));



		$valid->set_rules('password','Password','required|min_length[6]', 

			array(	'required'		=>	'Password harus diisi',

					'min_length	'	=>	'Password minimal 6 Karakter'));





		if ($valid->run()=== FALSE) {

		// End Validasi



		$data = array(	'title'		=>	'Update Profile : '.$user->nama,

						'user'		=> 	$user,

						'isi'		=>	'admin/dasbor/profile');

		$this->load->view('admin/layout/wrapper', $data, FALSE);

		//Jika Tidak Error Maka Masuk Database

		}else{

			$i = $this->input;



			//jika password lebih dari 6 karakter

			if(strlen($i->post('password')) > 6) {

				$data = array(	'id_user'		=> $id_user,

								'nama'			=> $i->post('nama'),

								'email'			=> $i->post('email'),

								'password'		=> sha1($i->post('password')),

								'akses_level'	=> $i->post('akses_level'),

								'keterangan'	=> $i->post('keterangan'),

								'foto'			=> $i->post('foto'),

							);

		}else{

				$data = array(	'id_user'		=> $id_user,

								'nama'			=> $i->post('nama'),

								'email'			=> $i->post('email'),

								'akses_level'	=> $i->post('akses_level'),

								'keterangan'	=> $i->post('keterangan'),

								'foto'			=> $i->post('foto'),

							);

		}



			$this->user_model->edit($data);

			$this->session->set_flashdata('sukses', 'Profile Telah diupdate');

			redirect(base_url('admin/dashboard'),'refresh');

		}

	// 	//End Validasi

	}

}



/* End of file Dasbor.php */

/* Location: ./application/controllers/admin/Dasbor.php */