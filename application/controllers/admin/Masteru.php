<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Masteru extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('pdf');
        $this->load->library('ciqrcode');
        $this->load->model('model_qrcode');
    }

    public $data = array(

    );

    // public $startNup;
    // public $endNup;

    public function index()
    {
        $data = array('title' => 'Data Aset',
            'isi' => 'admin/qrcode/list',
        );

        if (isset($_GET['filter'])) {
            if ($this->input->get('kd_brg')) {
                $data['kd_brg'] = $this->input->get('kd_brg');
            } else {
                $data['kd_brg'] = '-';
            }
            if ($this->input->get('startNup')) {
                $data['startNup'] = $this->input->get('startNup');
            } else {
                $data['startNup'] = '-';
            }
            if ($this->input->get('endNup')) {
                $data['endNup'] = $this->input->get('endNup');
            } else {
                $data['endNup'] = '-';
            }
            // if ($this->input->get('kd_brg') && $this->input->get('startNup')) {
            //     $this->session->set_flashdata('err', '<div class="alert alert-danger">Nup Akhir harus di isi</div>');
            //     redirect(base_url('admin/masteru'));
            // }
            if ($this->input->get('endNup') < $this->input->get('startNup')) {
                $this->session->set_flashdata('flash', '<div class="alert alert-danger">Sesuaikan nup awal dan akhir</div>');
                redirect(base_url('admin/masteru'));
            }
        } else {
            $data['kd_brg'] = '-';
            $data['startNup'] = '-';
            $data['endNup'] = '-';
        }

        $this->load->view('admin/layout/wrapper', $data);
    }

    public function qrCode($kodenya)
    {
        Qrcode::png(
            $kodenya,
            $outfile = false,
            $level = QR_ECLEVEL_H,
            $size = 5,
            $margin = 2
        );
    }

    public function simpan()
    {
        $kd_barang = $this->input->post('kd_barang');

        $this->load->library('ciqrcode'); //pemanggilan library QR CODE

        $config['cacheable'] = true; //boolean, the default is true
        $config['cachedir'] = './assets/'; //string, the default is application/cache/
        $config['errorlog'] = './assets/'; //string, the default is application/logs/
        $config['imagedir'] = './assets/images/'; //direktori penyimpanan qr code
        $config['quality'] = true; //boolean, the default is true
        $config['size'] = '1024'; //interger, the default is 1024
        $config['black'] = array(224, 255, 255); // array, default is array(255,255,255)
        $config['white'] = array(70, 130, 180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name = $kd_barang . '.png'; //buat name dari qr code sesuai dengan kd_barang

        $params['data'] = $kd_barang; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        $this->mahasiswa_model->simpan_mahasiswa($kd_barang); //simpan ke database
        redirect(base_url('admin/masteru')); //redirect ke mahasiswa usai simpan data
    }

    public function Edit($id)
    {
        // $qrcode = $this->model_qrcode->detail($id_qrcode);

        //validasi
        $valid = $this->form_validation;
        $valid->set_rules('ur_baru', 'Ur Baru', 'required',
            array('required' => 'Ur Baru Harus Di isi'));

        $valid->set_rules('merk_type', 'Merek', 'required',
            array('required' => 'Merek Tidak Boleh Kosong'));

        $valid->set_rules('asal_perlh', 'Asal Perolehan', 'required',
            array('required' => 'Asal Perolehan Tidak Boleh Kosong'));

        $valid->set_rules('keterangan', 'Keterangan Barang', 'required',
            array('required' => 'Keterangan Tidak Boleh Kosong'));

        if ($valid->run()) {

            $config['upload_path'] = './<?php  echo base_url()?>assets/admin/upload/images/'; //lokasi folder upload
            $config['allowed_types'] = 'gif|jpg|png|svg|tiff|jpeg'; //type file yang diijinkan
            $config['max_size'] = '12000'; // KB ukuran maksimal
            $this->load->library('upload', $config);

            $i = $this->input;
            // $data= array();
            $data = array(
                'id' => $id,
                'ur_baru' => $i->post('ur_baru'),
                'merk_type' => $i->post('merk_type'),
                'asal_perlh' => $i->post('asal_perlh'),
                'keterangan' => $i->post('keterangan'),
            );

            // print_r($data);die;

            if ($this->upload->do_upload('gambar')) {

                $upload_data = array('uploads' => $this->upload->data());
                // Image Editor
                $config['image_library'] = 'gd2';
                $config['source_image'] = './<?php  echo base_url()?>assets/admin/upload/images/' . $upload_data['uploads']['file_name'];
                $config['new_image'] = './<?php  echo base_url()?>assets/admin/upload/images/thumbs/';
                $config['create_thumb'] = true;
                $config['quality'] = "100%";
                $config['maintain_ratio'] = true;
                $config['width'] = 360; // Pixel
                $config['height'] = 360; // Pixel
                $config['x_axis'] = 0;
                $config['y_axis'] = 0;
                $config['thumb_marker'] = '';
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $data['foto'] = $upload_data['uploads']['file_name'];

            }

            $this->model_qrcode->edit($data);
       
            redirect(base_url('admin/masteru'), 'refresh');

        } else {
            $asset = $this->model_qrcode->detail($id);

            $data = array('title' => 'Edit Asset',
                'item' => $asset,
                'isi' => 'admin/qrcode/edit',
            );
            $this->load->view('admin/layout/wrapper', $data);
        }

    }

    public function delete($id)
    {

        //Proteksi sebelum di hapus
        if ($this->session->userdata('username') == "" && $this->session->userdata('akses_level') == "") {
            $this->session->set_flashdata('gagal', 'Silahkan Login terlebih dahulu');
            redirect(base_url('login'), 'refresh');
        }
        //End Proteksi

        $qrcode = $this->model_qrcode->detail($id);

        //hapus gambar
        if ($qrcode->qr_code != "") {

            unlink('./assets/images/' . $qrcode->qr_code);
        }

        $data = array('id' => $id,
        );
        $this->model_qrcode->delete($data);
        $this->session->set_flashdata('sukses', 'Data Berhasil Dihapus');
        redirect(base_url('admin/masteru'));
    }

    // include('vendor/qr-code/src/QrCode.php');
    // use src\QrCode\Endroid\QrCode\QrCode;
    public function coba_qrcode()
    {
        $params['data'] = 'This is a text to encode become Masteru';
        $params['level'] = 'H';
        $params['size'] = 10;
        $params['savename'] = 'assets/admin/upload/qr/' . 'wkwk.png';
        $this->ciqrcode->generate($params);

        echo '<img src="' . base_url('assets/admin/upload/qr/') . '" />';
    }

    public function print_6x3()
    {
        $startNup = $this->input->get('startNup', true);
        $endNup = $this->input->get('endNup', true);
        $kd_brg = $this->input->get('kd_brg', true);

        if ($startNup != '' && $endNup != '' && $endNup >= $startNup) {
            $param = array('no_aset>=' => $startNup, 'no_aset<=' => $endNup);

            if ($kd_brg != '') {
                $param['kd_brg'] = $kd_brg;
            }

            $qrcode = $this->model_qrcode->where($param);

            // $qrcode = $this->model_qrcode->listing(array($startNup, $endNup));

            $data = array('title' => 'Preview Label',
                'qrcode' => $qrcode,
                // 'isi' => 'admin/qrcode/list',
            );
            // $this->load->view('admin/pdf/print63', $data);
            $this->load->library('pdf');
            $customPaper = array(0, 0, 170.079, 85.0394);
            $this->pdf->setPaper($customPaper, 'portrait');
            $this->pdf->filename = "laporan.pdf";
            $this->pdf->load_view('laporan_pdf', $data);
        } elseif ($kd_brg != null) {
            $qrcode = $this->model_qrcode->barang($kd_brg);
            $data = array('title' => 'Data Masteru',
                'qrcode' => $qrcode,
                // 'isi' => 'admin/qrcode/list',
            );
            // $this->load->view('admin/pdf/print63', $data);
            $this->load->library('pdf');
            $customPaper = array(0, 0, 170.079, 85.0394);
            // $customPaper = array(0, 0, 2437.80, 3458.27);
            $this->pdf->setPaper($customPaper);
            $this->pdf->filename = "laporan.pdf";
            $this->pdf->load_view('laporan_pdf', $data);
        } else {
            // print_r('isi dulu lah');die;
            $data = array('title' => 'Data Masteru',
                'isi' => 'admin/qrcode/list',
            );

            $this->load->view('admin/layout/wrapper', $data);
        }
        
    }

    public function print_8x2()
    {
        // $qrcode = $this->model_qrcode->listing();
        // index();
        $startNup = $this->input->get('startNup', true);
        $endNup = $this->input->get('endNup', true);
        $kd_brg = $this->input->get('kd_brg', true);

        if ($startNup != '' && $endNup != '' && $endNup >= $startNup) {
            $param = array('no_aset>=' => $startNup, 'no_aset<=' => $endNup);

            if ($kd_brg != '') {
                $param['kd_brg'] = $kd_brg;
            }

            $qrcode = $this->model_qrcode->where($param);

            $data = array('title' => 'Preview Label',
                'qrcode' => $qrcode,
                // 'isi' => 'admin/qrcode/list',
            );
            $this->load->library('pdf');
            $customPaper = array(0, 0, 226.772, 56.6929);
            $this->pdf->setPaper($customPaper, 'portrait');
            $this->pdf->filename = "laporan.pdf";
            $this->pdf->load_view('laporan_pdf82', $data);
            // $this->load->view('admin/pdf/print82', $data);
        } elseif ($kd_brg != null) {
            $qrcode = $this->model_qrcode->barang($kd_brg);
            $data = array('title' => 'Data Masteru',
                'qrcode' => $qrcode,
                // 'isi' => 'admin/qrcode/list',
            );
            $this->load->library('pdf');
            $customPaper = array(0, 0, 226.772, 56.6929);
            $this->pdf->setPaper($customPaper, 'portrait');
            $this->pdf->filename = "laporan.pdf";
            $this->pdf->load_view('laporan_pdf82', $data);
            // $this->load->view('admin/pdf/print63', $data);
        } else {
            // print_r('isi dulu lah');die;
            $data = array('title' => 'Data Masteru',
                'isi' => 'admin/qrcode/list',
            );

            $this->load->view('admin/layout/wrapper', $data);
        }
    }

    public function laporan_pdf()
    {
        // $kd_barang = $this->input->post('kd_brg');
        $qrcode = $this->model_qrcode->listing();
        $data = array('title' => 'Data Masteru',
            'isi' => 'admin/qrcode/list',
            'qrcode' => $qrcode,
            // 'bismillah' => $kd_barang,
        );

        $this->load->library('pdf');
        $customPaper = array(20, -40, 300, 200);
        // $customPaper = array(0, 0, 2437.80, 3458.27);
        $this->pdf->setPaper($customPaper, 'portrait');
        $this->pdf->filename = "laporan.pdf";
        $this->pdf->load_view('laporan_pdf', $data);

    }

    public function laporan_pdf82()
    {
        $qrcode = $this->model_qrcode->listing();
        $data = array('title' => 'Data Masteru',
            'isi' => 'admin/qrcode/list',
            'qrcode' => $qrcode,
        );

        $this->load->library('pdf');
        $customPaper = array(-40, -30, 300, 160);
        $this->pdf->setPaper($customPaper, 'portrait');
        $this->pdf->filename = "laporan.pdf";
        $this->pdf->load_view('laporan_pdf82', $data);

    }
}
