<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index()
    {
        $users = $this->user_model->listing();

        $data = array(
            'title' => 'Data User',
            'items' => $users,
            'isi' => 'admin/user/list',
        );
        $this->load->view('admin/layout/wrapper', $data);
    }

    public function tambah()
    {
        $config['upload_path'] = './<?php  echo base_url()?>assets/admin/upload/images/'; //lokasi folder upload
        $config['allowed_types'] = 'gif|jpg|png|svg|tiff|jpeg'; //type file yang diijinkan
        $config['max_size'] = '12000'; // KB ukuran maksimal
        $this->load->library('upload', $config);

        $i = $this->input;

        $data = array( //'slug__kategori_user'        => $slug,
            'nama' => $i->post('nama'),
            'email' => $i->post('email'),
            'username' => $i->post('username'),
            'password' => sha1($i->post('password')),
            // 'nama_id'            => $this->session->userdata('nama_id')
        );

        if ($this->upload->do_upload('gambar')) {

            $upload_data = array('uploads' => $this->upload->data());
            // Image Editor
            $config['image_library'] = 'gd2';
            $config['source_image'] = './<?php  echo base_url()?>assets/admin/upload/images/' . $upload_data['uploads']['file_name'];
            $config['new_image'] = './<?php  echo base_url()?>assets/admin/upload/images/thumbs/';
            $config['create_thumb'] = true;
            $config['quality'] = "100%";
            $config['maintain_ratio'] = true;
            $config['width'] = 360; // Pixel
            $config['height'] = 360; // Pixel
            $config['x_axis'] = 0;
            $config['y_axis'] = 0;
            $config['thumb_marker'] = '';
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            $data['foto'] = $upload_data['uploads']['file_name'];
        }

        $this->user_model->tambah($data);
        $this->session->set_flashdata('sukses', 'Data Berhasil ditambahkan');
        redirect(base_url('admin/user'), 'refresh');

    }

    public function Edit($nama_id)
    {
//validasi
        $valid = $this->form_validation;
        $valid->set_rules('nama', 'Nama', 'required',
            array('required' => 'Nama Harus Di isi'));

        $valid->set_rules('email', 'Email', 'required',
            array('required' => 'Tidak Boleh Kosong'));

        $valid->set_rules('username', 'Username', 'required',
            array('required' => 'Tidak Boleh Kosong'));

        // $valid->set_rules('password', 'Password', 'required',
        //     array('required' => 'Tidak Boleh Kosong'));

        if ($valid->run()) {

            $config['upload_path'] = './<?php  echo base_url()?>assets/admin/upload/images/'; //lokasi folder upload
            $config['allowed_types'] = 'gif|jpg|png|svg|tiff|jpeg'; //type file yang diijinkan
            $config['max_size'] = '12000'; // KB ukuran maksimal
            $this->load->library('upload', $config);

            $i = $this->input;

            $data = array(
                'nama_id' => $nama_id,
                'nama' => $i->post('nama'),
                'email' => $i->post('email'),
                'username' => $i->post('username'),
                'password' => sha1($i->post('password')),
            );

            if ($this->upload->do_upload('gambar')) {

                $upload_data = array('uploads' => $this->upload->data());
                // Image Editor
                $config['image_library'] = 'gd2';
                $config['source_image'] = './<?php  echo base_url()?>assets/admin/upload/images/' . $upload_data['uploads']['file_name'];
                $config['new_image'] = './<?php  echo base_url()?>assets/admin/upload/images/thumbs/';
                $config['create_thumb'] = true;
                $config['quality'] = "100%";
                $config['maintain_ratio'] = true;
                $config['width'] = 360; // Pixel
                $config['height'] = 360; // Pixel
                $config['x_axis'] = 0;
                $config['y_axis'] = 0;
                $config['thumb_marker'] = '';
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $data['foto'] = $upload_data['uploads']['file_name'];

            }

            $this->user_model->edit($data);
            // $this->session->set_flashdata('sukses', 'Data User B');
            redirect(base_url('admin/user'), 'refresh');

        } else {
            $user = $this->user_model->detail($nama_id);

            $data = array('title' => 'Edit User',
                'item' => $user,
                'isi' => 'admin/user/edit',
            );
            $this->load->view('admin/layout/wrapper', $data);
        }

    }

    public function delete($nama_id)
    {

        $user = $this->user_model->detail($nama_id);

        //hapus gambar
        // if ($user[0]->foto != "") {

        //     unlink('./assets/admin/upload/images/user/' . $user[0]->foto);
        //     unlink('./assets/admin/upload/images/user/thumbs/' . $user[0]->foto);
        // }

        $data = array('nama_id' => $nama_id);
        $this->user_model->delete($data);
        $this->session->set_flashdata('Sukses', 'Data telah di hapus');
        redirect(base_url('admin/user'));
    }
}

/* End of file user.php */
