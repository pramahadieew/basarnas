<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Scanner extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_scan');
        // $this->load->model('model_qrcode');
    }

    public function index()
    {
        $qrcode = $this->model_scan->listing();
        header('Content-Type: application/json');
        json_encode($qrcode, true);
    }
}

/* End of file user.php */
