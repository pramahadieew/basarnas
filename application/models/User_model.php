<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->database();
        // $db2 = $this->load->database('db2', TRUE);

    }

    // public function save_upload($NAMA,$IMAGE){
    //     $data = array(
    //             'NAMA'     => $NAMA,
    //             'FILE_KTP' => $IMAGE
    //         );
    //     $result= $this->db->insert('gallery',$data);
    //     return $result;
    // }

    //listing
    public function listing()
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->order_by('nama_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    //Detail
    public function detail($nama_id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('nama_id', $nama_id);
        $this->db->order_by('nama_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    //Login Function
    public function login($username, $password)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where(array('username' => $username,
            'password' => $password));
        $this->db->order_by('nama_id', 'ASC');
        $query = $this->db->get();
        return $query->row();
    }

    //Tambah Data User
    public function tambah($data)
    {

        $this->db->insert('user', $data);

    }

    //Edit User
    public function edit($data)
    {

        $this->db->where('nama_id', $data['nama_id']);
        $this->db->update('user', $data);
    }

    //Hapus User
    public function delete($data)
    {

        $this->db->where('nama_id', $data['nama_id']);
        $this->db->delete('user', $data);
    }

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */
