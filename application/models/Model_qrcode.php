<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_qrcode extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function where($parameter = null)
    {
        $this->db->from('report');
        $this->db->where($parameter);
        $query = $this->db->get();
            
        return $query->result();
    }

    //listing + filter
    public function listing($param = null)
    {

        if ($param != null) {
            $this->db->from('report');
            $this->db->where('nup>=', $param[0]);
            $this->db->where('nup<=', $param[1]);

            $query = $this->db->get();
            
            return $query->result();
        } else {
            // $this->db->select(YEAR('tahun_pembelian'));
            $this->db->from('report');

            $query = $this->db->get();
            return $query->result();
        }

        // return $this->db->last_query();
    }
    //filter
    public function barang($kd_brg = null)
    {
        $this->db->from('report');
        $this->db->where('kd_brg=', $kd_brg);
        $query = $this->db->get();
            
        return $query->result();
    }
    //filter
    public function nup($nup = null)
    {
        $this->db->from('report');
        $this->db->where('no_aset=', $nup);
        $query = $this->db->get();
            
        return $query->result();
    }

    //filter
    public function satker($kd_satker = null)
    {
        $this->db->from('report');
        $this->db->where('kd_satker=', $kd_satker);
        $query = $this->db->get();
            
        return $query->result();
    }

    //filter
    public function tahun($tahun_pembelian = null)
    {
        $this->db->from('report');
        $this->db->where('year(tahun_pembelian)', $tahun_pembelian);
        // $sql = "select * from report where year(tahun_pembelian) = $tahun_pembelian";
        $query = $this->db->get();
            
        return $query->result();
    }

    // Check Judul qrcode
    public function check_judul($title)
    {
        $query = $this->db->get_where('qrcode', array('title' => $title));
        return $query->row();
    }

    //Detail
    public function detail($id)
    {
        $query = $this->db->get_where('report', array('id' => $id));
        return $query->result();
    }

    //Detail With Slug
    public function detail_slug($slug_qrcode)
    {
        $query = $this->db->get_where('qrcode', array('slug_qrcode' => $slug_qrcode));
        return $query->row();
    }

    // Menampilkand data qrcode
    public function daftar_qrcode($read = false)
    {
        if ($read === false) {
            $query = $this->db->query('SELECT * FROM qrcode WHERE status_qrcode ="Publish" ORDER BY id_qrcode DESC');
            return $query->result_array();
        }
        $query = $this->db->get_where('qrcode', array('slug_qrcode' => $read));
        return $query->row_array();
    }

    public function akhir()
    {
        $query = $this->db->query('SELECT * FROM qrcode ORDER BY id_qrcode DESC');
        return $query->row();

    }

    //Tambah Data qrcode
    public function tambah($data)
    {

        $this->db->insert('report', $data);

    }

    //Edit qrcode
    public function edit($data)
    {

        $this->db->where('id', $data['id']);
        $this->db->update('report', $data);
    }

    //Hapus qrcode
    public function delete($data)
    {

        $this->db->where('id', $data['id']);
        $this->db->delete('report', $data);
    }

 //    public function print($id)
 //    {
 //        $this->db->where('id', $id);
 //        $data = $this->db->get('report');
 //        $output = '<table width="100%" cellspacing="5" cellpadding="5">';
 //          foreach($data->result() as $row)
 //          {
 //           $output .= '
 //           <tr>
 //            <td width="25%"><img src="'.base_url().'assets/images/'.$row->qr_code.'" /></td>
 //            <td width="75%">
 //             <p><b>Kode : </b>'.$row->kode_barang.'</p>
 //             <p><b>Nama Barang : </b>'.$row->nama_barang.'</p>
 //             <p><b>Penyedia : </b>'.$row->nama_penyedia.'</p>
 //             <p><b>Merk : </b>'.$row->merek.'</p>
 //             <p><b>NUP : </b> '.$row->nup.' </p>
 //            </td>
 //           </tr>
 //           ';
 //          }
 //          $output .= '
 //          <tr>
 //           <td colspan="2" align="center"><a href="'.base_url().'htmltopdf" class="btn btn-primary">Back</a></td>
 //          </tr>
 //          ';
 //          $output .= '</table>';
 //          return $output;
 // }
 //    }
}

/* End of file qrcode_model.php */
/* Location: ./application/models/qrcode_model.php */
