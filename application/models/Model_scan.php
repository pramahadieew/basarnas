<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_scan extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //listing + filter
    public function listing($scan = null)
    {
        $this->db->from('report');
        // $this->db->where('kd_barang=', $scan);
        // $this->db->where('nup=', $scan);
        if($scan)
        {
            $this->db->where($scan);
        }

        $query = $this->db->get();

        return $query->result();

        // return $this->db->last_query();
    }
}
