<p>
	<!-- <a
		href="<?php echo base_url('admin/masteru/tambah') ?>"
		class="btn btn-primary"
		><i class="fa fa-plus"></i> Tambah</a
	> -->
	<button class="btn btn-success filter" id="" onclick="print()"
		><i class="fa fa-print"></i> Print 6x3
	</button>

</p>

<?php
//--Notifikasi..
if ($this->session->flashdata('sukses')) {echo '
<div class="alert alert-success">
	<i class="fa fa-check"></i> ';
    echo $this->session->flashdata('sukses');
    echo
        '
</div>
';}?>

	<form action="<?=site_url('admin/masteru')?>" class="" method="get">
	<div class="row">
		<div class="col-md-2">
            <i class="fa fa-calendar"></i>
            <label>Start Date</label>
			<input type="text" name="startdate" id="startdate" class="form-control" autocomplete="off" />
		</div>
		<div class="col-md-2">
            <i class="fa fa-calendar"></i>
			<label>End Date</label>
			<input type="text" name="enddate" id="enddate" class="form-control" autocomplete="off" />
		</div>
		<div class="col-md-2">
        <i class="fa fa-cube"></i>
			<label>Kode Barang	</label>
			<select class="form-control select2" id="kd_brg" name="kd_brg" style="width: 100%;">
                <option selected="selected"></option>
                <?php foreach ($listing as $rows) {?>
					<option value="<?=$rows->kd_barang?>" <?php if($kd_brg==$rows->kd_barang) { echo 'selected'; } ?>>
					<?php echo $rows->kd_barang ?>
					</option>
					<?php }?>
			</select>
		</div>
		<!-- <?=$listing?> -->
		<!-- <div class="col-md-2">
        <i class="fa fa-cube"></i>
			<label>Kode Barang</label>
			<input type="text" name="kd_brg" id="kd_brg" class="form-control" />
		</div> -->
		<!-- <div class="col-md-1">
        <i class="fa fa-key"></i>
			<label>NUP</label>
			<input type="text" name="nup" id="nup" class="form-control" />
		</div> -->
		<div class="col-md-2">
        <i class="fa fa-key"></i>
			<label>NUP	</label>
			<select class="form-control select2" id="nup" name="nup" style="width: 100%;">
                <option selected="selected"></option>
                <?php foreach ($listing as $rows) {?>
					<option value="<?=$rows->nup?>" <?php if($nup==$rows->nup) { echo 'selected'; } ?>>
					<?php echo $rows->nup ?>
					</option>
					<?php }?>
			</select>
		</div>
		<!-- <div class="col-md-2">
        <i class="fa fa-lock"></i>
			<label>Satker</label>
			<input type="text" name="kd_satker" id="satker" class="form-control" />
		</div> -->
		<div class="col-md-2">
        <i class="fa fa-lock"></i>
			<label>Satker</label>
			<select class="form-control select2" id="satker" name="kd_satker" style="width: 100%;">
                <option selected="selected"></option>
                <?php foreach ($listing as $rows) {?>
					<option value="<?=$rows->kd_satker?>" <?php if($kd_satker==$rows->kd_satker) { echo 'selected'; } ?>>
					<?php echo $rows->kd_satker ?>
					</option>
					<?php }?>
			</select>
		</div>
		<!-- /.form-group -->
		<!-- <div class="col-md-2">
        <i class="fa fa-circle"></i>
			<label>Tahun</label>
			<select class="form-control select2" id="tahun" name="tahun_pembelian" style="width: 100%;">
                <option selected="selected"></option>
                <?php
for ($year = 2000; $year <= 2050; $year++) {
    $selected = (isset($getYear) && $getYear == $year) ? 'selected' : '';
    echo "<option value=$year $selected>$year</option>";
}
?>
			</select>
		</div> -->
		<!-- /.form-group -->
		<div class="col-md-2">
        <i class="fa fa-filter"></i>
			<label>Action</label>
			<br />
			<button type="submit" id="" class="btn btn-success filter">Filter</button>
			<!-- <input type="button" name="filter" id="filter" value="Filter" class="btn btn-success"> -->
		</div>
	</div>
</form>

<br />

<div class="row">
	<div class="col-md-12">
		<div class="hack1">
			<div class="hack2">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th width="10%">#</th>
							<th>Kode Satker</th>
							<th>Kode Barang</th>
							<th>Nama Barang</th>
							<th>Tanggal Pembelian</th>
							<th>NUP</th>
							<th>Merek/Type</th>
							<!-- <th>Type</th> -->
							<th>Nama Penyedia</th>
							<th>Nilai Barang</th>
							<th>Keterangan Barang</th>
							<th>Lokasi</th>
							<th>QR Code</th>
							<!-- <th width="20%">Action</th> -->
						</tr>
					</thead>
					<tbody>
						<?php $i = 1;foreach ($qrcode as $qrcode) {?>

						<tr>
							<td><?php echo $i ?></td>
							<td><?php echo $qrcode->kd_satker ?></td>
							<td><?php echo $qrcode->kd_barang ?></td>
							<td><?php echo $qrcode->nama_barang ?></td>
							<td><?php echo $qrcode->tahun_pembelian ?></td>
							<td><?php echo $qrcode->nup ?></td>
							<td><?php echo $qrcode->merek ?></td>
							<!-- <td><?php echo $qrcode->type ?></td> -->
							<td><?php echo $qrcode->nama_penyedia ?></td>
							<td><?php echo $qrcode->nilai_barang ?></td>
							<td><?php echo $qrcode->keterangan_barang ?></td>
							<td><?php echo $qrcode->lokasi ?></td>
							<td>
								<img
									src="<?=site_url('admin/masteru/qrCode/' . $qrcode->kd_barang . '-' . $qrcode->nup);?>"
									alt=""
								/>
							</td>

							<!-- <td>
								<a
									href="<?php echo base_url('admin/qrcode/edit/' . $qrcode->id) ?>"
									class="btn btn-warning btn-xs"
									><i class="fa fa-edit"></i></a
								>

								<?php include 'delete.php';?>
							</td> -->
						</tr>
						<?php $i++;}?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function print() {
	var startdate =  document.getElementById("startdate");
	var enddate =  document.getElementById("enddate");
	var kd_brg =  document.getElementById("kd_brg");
	var nup =  document.getElementById("nup");
	var satker =  document.getElementById("satker");

    // if (startdate != '' && enddate != '') {

		window.location.href="<?=base_url()?>admin/masteru/print_6x3?startdate="+ startdate.value +"&enddate="+ enddate.value  +"&kd_brg="+ kd_brg.value  +"&nup="+ nup.value  +"&satker="+ satker.value;
	// } else {
	// 	window.location.href="masteru";
	// }
}
</script>
