<?php
// Proteksi Halaman Admin dengan Login
if ($this->session->userdata('username') == "" && $this->session->userdata('level') == "") {
    $this->session->set_flashdata('gagal', 'Silahkan Login terlebih dahulu');
    redirect(base_url('AuthAdmin'), 'refresh');
}
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
 <title><?php echo $title; ?></title>
  <link rel="icon" href="<?php echo base_url(); ?>assets/admin/images/logo-sar.jpg">
<!--   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
<style>
body {
  margin:0;
  padding: 0;
}
* {
  box-sizing: border-box;
}

.column {
  float: left;
  width: 76px;
  padding: 5px;
}

.row:after {
  content: "";
  display: table;
  clear: both;

}

.sizeFont {
  font-size: 11px;
  /* min-height: -100px; */
  /* left: 100px; */
  /*top: 40;*/
}

html{margin:0px 10px}

.smFont {
  font-size: 8px;
}
</style>
</head>

<body>
<?php $i = 1;foreach ($qrcode as $qrcode) {?>
<table border="0" width="100%">
  <tr>
    <th width="30%"></th>
    <th style="text-align:left" class="sizeFont"><?=$qrcode->kd_lokasi?></th>
    <th></th>
  </tr>
  <tr>
    <th width="30%"></th>
    <th style="text-align:left" class="sizeFont"><?=$qrcode->kd_brg?></th>
    <th></th>
    <th class="sizeFont"><?=substr($qrcode->tgl_perlh, 0, 4);?></th>
  </tr>
   <tr>
    <th width="30%"></th>
    <th style="text-align:left" class="smFont"><?=$qrcode->ur_baru?></th>
    <th></th>
    <th class="sizeFont"><?=$qrcode->no_aset?></th>
  </tr>
   <tr>
    <th style="text-align:left" class="sizeFont">KP BSN</th>
    <th><img src="assets/images/<?=$qrcode->qr_code?>" width='40'></th>
    <th></th>
  </tr>
</table>
<?php $i++;}?>
<script>
// var startNup   =  document.getElementById("startNup").value;
// const product = urlParams.get('startNup')
//   console.log(product);

</script>
</body>
</html>
