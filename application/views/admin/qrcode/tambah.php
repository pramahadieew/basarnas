<?php
//validasi input
echo validation_errors('<div class= "alert alert-warning">', '</div>');

//error upload file
if (isset($error)) {
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}
//form open
echo form_open_multipart(base_url('admin/masteru/tambah'));
?>
<?php
if ($this->session->flashdata('gagal')) {
    echo '<div class="alert alert-danger"><i class="fa fa-warning"></i> ';
    echo $this->session->flashdata('gagal');
    echo '</div>';
}
?>

	<div class="col-md-12">
		<div class="form-group">
			<label>Title</label>
			<input type="text" name="title" class="form-control" placeholder="Title" value="<?php echo set_value('title') ?>"required>
		</div>
	</div>

	<div class="col-md-12">
		<div class="form-group">
			<label>File Xls</label>
			<input type="file" name="file_xls" class="form-control">
		</div>
	</div>


	<!--<div class="col-md-12">
		<div class="form-group form-group-lg">
			<label>Isi Berita</label>
			<textarea name="isi" class="form-control" placeholder="Isi Berita"><?php echo set_value('isi') ?></textarea>
		</div> -->


<div class="col-md-12">
	<div class="form-group">
      <input type="submit" name="submit" class="btn btn-success" value="Save">
      <input type="reset" name="reset" class="btn btn-default" value="Reset">
    </div>
</div>
<?php echo form_close(); ?>