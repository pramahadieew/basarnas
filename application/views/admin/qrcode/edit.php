<div class="col-md-12">
  <?php

echo validation_errors('<div class="alert alert-warning">', '</div>');

//error upload file
if (isset($error)) {
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

echo form_open_multipart(base_url('admin/masteru/edit/' . $item[0]->id));
?>
  <?php foreach ($item as $asset) {?>

  <div class="form-group">
    <label>Nama Barang</label>
    <input type="text" name="ur_baru" class="form-control" value="<?php echo $asset->ur_baru; ?>" placeholder='Nama Barang'
      required>
  </div>
</div>

<div class="col-md-12">
  <div class="form-group">
    <label>Merk/Type</label>
    <input type="text" name="merk_type" class="form-control" value="<?php echo $asset->merk_type; ?>"
      placeholder='Masukan Merk' required>
  </div>
</div>

<div class="col-md-12">
  <div class="form-group">
    <label>Asal Perolehan</label>
    <input type="text" name="asal_perlh" class="form-control" value="<?php echo $asset->asal_perlh; ?>" placeholder='Masukan Nama Penyedia'
      required>
  </div>
</div>

<div class="col-md-12">
  <div class="form-group">
    <label>Keterangan</label>
    <input type="text" name="keterangan" class="form-control" value="<?php echo $asset->keterangan; ?>" placeholder='' >
  </div>
</div>
<?php ;}?>

<div class="col-md-12">
<div class="form-group">
  <input type="submit" name="submit" class="btn btn-success" value="Simpan Data">
  <input type="reset" name="reset" class="btn btn-default" value="Reset">
</div>
</div>

<?php echo form_close(); ?>