<p>
<?php echo $this->session->flashdata('notif') ?>
  <button type="submit" class="btn btn-success filter" id="" onclick="print63()"><i class="fa fa-print"></i> Print 6x3</button>
  <button type="submit" class="btn btn-success filter" id="" onclick="print82()"><i class="fa fa-print"></i> Print 8x2</button>
  <button type="submit" class="btn btn-default" data-toggle="modal" data-target="#modal-default"><i class="fa fa-file-excel-o"></i> Import</button>
</p>

<?php
//--Notifikasi..
if ($this->session->flashdata('sukses')) {echo '
<div class="alert alert-success">
	<i class="fa fa-check"></i> ';
    echo $this->session->flashdata('sukses');
    echo
        '
</div>
';}?>
     <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                <div class="col-md-4 offset-2">
            <?php //echo $this->session->flashdata('notif') ?>
            <form method="POST" action="<?php echo base_url() ?>index.php/import/upload" enctype="multipart/form-data">
              <div class="form-group">
                <label for="exampleInputEmail1">UNGGAH FILE EXCEL <sup>*xlsx</sup></label>
                <input type="file" name="userfile" class="form-control">
              </div>

              <button type="submit" class="btn btn-success">Save</button>
            </form>
        </div>
              </div>
              <div class="modal-footer">
              </div>
            </div>

          </div>

        </div>
        <!-- /.modal -->
	<form action="<?=site_url('admin/masteru/index')?>" class="" method="get">
	<!-- <input type="hidden" name="<?php //echo $this->security->get_csrf_token_name(); ?>" value="<?php //echo $this->security->get_csrf_hash(); ?>"> -->
	<div class="row">
    <div class="col-md-2">
        <i class="fa fa-cube"></i>
			<label>Kode Barang</label>
			<input type="text" name="kd_brg" id="kd_brg" class="form-control" value="<?php if ($kd_brg != '-') {echo $kd_brg;}?>" />
		</div>
		<div class="col-md-2">
        <i class="fa fa-key"></i>
			<label>No Aset Awal</label>
			<input type="text" name="startNup" id="startNup" class="form-control" value="<?php if ($startNup != '-') {echo $startNup;}?>" />
    <?php echo $this->session->flashdata('flash') ?>
		</div>
		<div class="col-md-2">
        <i class="fa fa-key"></i>
			<label>No Aset Akhir</label>
			<input type="text" name="endNup" id="endNup" class="form-control" value="<?php if ($endNup != '-') {echo $endNup;}?>" />
    <?php //echo $this->session->flashdata('err') ?>
		</div>
		<!-- /.form-group -->
		<div class="col-md-2">
        <i class="fa fa-print"></i>
			<label>Action</label>
			<br />
			<button type="submit" id="filter" name="filter" value="filter" class="btn btn-success filter">Filter</button>
			<div class="row">

		</div>
			</div>

		</div>
</form>

<br />
<div class="row">
	<div class="col-md-12">
		<div class="hack1">
			<div class="hack2">
				    <table id="myTable" class="display table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                      <th>#</th>
                      <th>Kode Lokasi</th>
                      <th>Kode Barang</th>
                      <th>No Aset</th>
                      <th>Ur Baru</th>
                      <th>Tgl Perolehan</th>
                      <th>Merek/Type</th>
                      <th>Asal</th>
                      <th>Rph Satuan</th>
                      <th>Keterangan</th>
                      <th>No. Mesin</th>
                      <th>No. Rangka</th>
                      <th>No. Polisi</th>
                      <th>No. BPKB</th>
                      <th>Action</th>
                    </tr>
                </thead>
              <tbody>
              </tbody>
            </table>
			</div>
		</div>
	</div>
</div>

<!-- jsprint form -->
<script type="text/javascript">
function print82() {
	var startNup   =  document.getElementById("startNup").value;
	var endNup     =  document.getElementById("endNup").value;
    var kd_brg     =  document.getElementById("kd_brg").value;

    if (startNup != '' && endNup != '' && endNup >= startNup) {
    alert('Print label 8x2?');
        window.location.href="<?=base_url()?>admin/masteru/print_8x2?kd_brg="+ kd_brg +"&startNup="+ startNup +"&endNup="+ endNup;
	} else if(kd_brg != '') {
        window.location.href="<?=base_url()?>admin/masteru/print_8x2?kd_brg="+ kd_brg;
    } else {
    alert('Mohon Cek Form NUP awal dan akhir');
		window.location.href="<?=base_url()?>admin/masteru";
	}
}

function print63() {
	var startNup   =  document.getElementById("startNup").value;
	var endNup     =  document.getElementById("endNup").value;
  var kd_brg     =  document.getElementById("kd_brg").value;

    if (startNup != '' && endNup != '' && endNup >= startNup) {
      alert('Print label 6x3?');
		window.location.href="<?=base_url()?>admin/masteru/print_6x3?kd_brg="+ kd_brg +"&startNup="+ startNup +"&endNup="+ endNup;
	} else if(kd_brg != '') {
        window.location.href="<?=base_url()?>admin/masteru/print_6x3?kd_brg="+ kd_brg;
  } else {
    alert('Mohon Cek Form NUP awal dan akhir');
		window.location.href="<?=base_url()?>admin/masteru";
	}
}
</script>



<!-- search+datatable -->
<script type="text/javascript">
  $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var startNup = parseInt( $('#startNup').val(), 10 );
        var endNup = parseInt( $('#endNup').val(), 10 );
        var nup = parseFloat( data[5] ) || 0; // use data for the nup column

        if ( ( isNaN( startNup ) && isNaN( endNup ) ) ||
             ( isNaN( startNup ) && nup <= endNup ) ||
             ( startNup <= nup   && isNaN( endNup ) ) ||
             ( startNup <= nup   && nup <= endNup ) )
        {
            return true;
        }
        return false;
    }
  );

 	var save_method; //for save method string
    var table;

    $(document).ready(function() {
        //datatables
        table = $('#myTable').DataTable({

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [],
             //Initial no order.
            // Load data for the table's content from an Ajax source
            <?php //if(isset($_GET['filter'])) { ?>
                "ajax": {"url": "<?php echo site_url('dummy/json') . '/' . $kd_brg . '/' . $startNup . '/' . $endNup ?>", "type": "POST"},
            <?php //} else { ?>
                // "ajax": {"url": "<?php //echo site_url('dummy/json');?>", "type": "POST"},
            <?php //} ?>

            //Set column definition initialisation properties.
            "columns": [
                {"data": "id"},
                {"data": "kd_lokasi"},
                {"data": "kd_brg"},
                {"data": "no_aset"},
                {"data": "ur_baru"},
                {"data": "tgl_perlh"},
                {"data": "merk_type"},
                {"data": "asal_perlh"},
                {"data": "rph_sat",
                  render: $.fn.dataTable.render.number( ',', '.', 2 )
                },
                {"data": "keterangan"},
                {"data": "no_mesin"},
                {"data": "no_rangka"},
                {"data": "no_polisi"},
                {"data": "no_bpkb"},
                {"data": "view"}
            ],

        });
      $('#startNup').keyup( function() { table.draw(); } );
      $('#endNup').keyup( function() { table.draw(); } );
	});

</script>
