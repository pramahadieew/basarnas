</div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.1
    </div>
    <strong>Copyright &copy; 2020  <a href="http://bgs-indo.com/">PT Bagas Gemilang Satwika</a>.</strong> All rights
    reserved.

    <!-- <br><strong>Editor Creative By Woyo Project <i class="fa fa-rocket"></i></strong> -->
  </footer>

<!-- ./wrapper -->


<!-- date-range-picker -->
<!-- <script src="<?php //echo base_url() ?>assets/admin/bower_components/moment/min/moment.min.js"></script>
<script src="<?php //echo base_url() ?>assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<!-- bootstrap datepicker -->
<script src="<?php echo base_url() ?>assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url() ?>assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>assets/admin/bower_components/fastclick/lib/fastclick.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url() ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/admin/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() ?>assets/admin/dist/js/demo.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<!-- pnup script -->
<script>
$(function() {
  // $('#example1').DataTable()
   //Initialize Select2 Elements
   // $('.select2').select2()
  //Date picker
  // $('.datepicker').datepicker({
  //     autoclose: true
  //   })
  })
</script>

  
 
</body>
</html>