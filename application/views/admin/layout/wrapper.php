<?php
// Proteksi Halaman Admin dengan Login 	
if($this->session->userdata('username') == "") {
	$this->session->set_flashdata('gagal', 'Silahkan Login terlebih dahulu');
	redirect(base_url('AuthAdmin'),'refresh');	
}


//gabungkan semua bagian layout
require_once('head.php');
require_once('header.php');
require_once('sidebar.php');
require_once('content.php');
require_once('footer.php');