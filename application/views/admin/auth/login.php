<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title ?></title>

 <!-- icon title -->
<link rel="icon" href="<?php echo base_url(); ?>assets/admin/images/logo-sar.png">
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/Ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/AdminLTE.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/square/blue.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/Ionicons/css/ionicons.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/custom.css">

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<style type="text/css">

</style>
</head>
<body class="hold-transition login-page bg-login back1">
<div class="login-box">
<div class="login-logo">
    <img src="<?php echo base_url(); ?>assets/admin/images/logo-sar.png" width="80px"><br>
<!-- <a href="#"><b>SAR</b> Administrator</a> -->
</div>
<!-- /.login-logo -->
<div class="login-box-body">
<p class="login-box-msg">Sign in to start your Administrator</p>

<?php

// Notif jika ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i> ', '</div>');

//--Notifikasi..
if ($this->session->flashdata('gagal')) {
    echo '<div class="alert alert-danger"><i class="fa fa-warning"></i> ';
    echo $this->session->flashdata('gagal');
    echo '</div>';
}

if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success"><i class="fa fa-check"></i> ';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}

?>

<form role="form" action="<?php echo base_url('AuthAdmin'); ?>" method="post">
<!-- <input type="hidden" name="<?php //echo $this->security->get_csrf_token_name(); ?>" value="<?php //echo $this->security->get_csrf_hash(); ?>"> -->
<div class="form-group input-group">
          <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
          <input type="text" class="form-control" placeholder="Your Username " name="username" />
      </div>
          <div class="form-group input-group">
          <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
          <input type="password" class="form-control"  placeholder="Your Password" name="password" />
      </div>

<div class="row">
<div class="col-xs-8">
<div class="checkbox icheck">
</div>
</div>

    <!-- /.col -->
    <div class="col-xs-4">
      <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
    </div>
    <!-- /.col -->
  </div>
</form>

<!-- /.social-auth-links -->

<!-- <a href="<?php //echo base_url('home') ?>" class="text-center">Click here for Homepage</a> -->

</div>
<!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/admin/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script>
$(function () {
$('input').iCheck({
  checkboxClass: 'icheckbox_square-blue',
  radioClass: 'iradio_square-blue',
  increaseArea: '20%' /* optional */
});
});
</script>
</body>
</html>
