<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">
  <i class="fa fa-plus"></i> Tambah
</button>


<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Users</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
          <?php

echo validation_errors('<div class="alert alert-warning">', '</div>');

//error upload file
if (isset($error)) {
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

echo form_open_multipart(base_url('admin/User/tambah'));
?>
          <div class="form-group">
            <label>Name User</label>
            <input type="text" name="nama" class="form-control" value="<?php echo set_value('nama'); ?>"
              placeholder='Nama User' required>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" value="<?php echo set_value('username'); ?>"
              placeholder='Masukan Username' required>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control" value="<?php echo set_value('email'); ?>"
              placeholder='Masukan Email' required>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label>Password User</label>
            <input type="password" name="password" class="form-control" value="<?php echo set_value('password'); ?>"
              placeholder='' required>
          </div>
        </div>

        <div class="col-md-12">
          <!-- <div class="form-group">
            <label>Foto User</label>
            <input type="file" name="gambar" class="form-control">
          </div> -->
        <div class="form-group">
          <input type="submit" name="submit" class="btn btn-success" value="Simpan Data">
          <input type="reset" name="reset" class="btn btn-default" value="Reset">
        </div>
        </div>
      
      </div>

      <?php echo form_close(); ?>
  

      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>

      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>