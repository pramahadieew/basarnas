<div style="margin-bottom: 10px">
    <?php include 'tambah.php';?>
</div>
<?php
//--Notifikasi..
if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success"><i class="fa fa-check"></i> ';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>

<table id="example1" class="table table-bordered table-striped">
<thead>
    <tr>
        <th width="5%">No</th>
        <th width="18%">Nama User</th>
        <th>Email</th>
        <th>Username</th>
        <!-- <th>Foto</th> -->
        <th>action</th>
    </tr>
</thead>
<tbody>
    <?php $i = 1; foreach ($items as $item) { ?>
        <tr>
        <td><?php echo $i ?></td>
        <td><?php echo $item->nama ?></td>
        <td><?php echo $item->email ?></td>
        <td><?php echo $item->username ?></td>
        <!-- <td> <img src="<?php //echo base_url('assets/admin/upload/images/user'.$item->foto) ?>" width='60'></td> -->
                <td>
            <a href="<?php echo base_url('admin/User/edit/'.$item->nama_id) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
    
    <?php include 'delete.php'; ?>
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>

</table>
