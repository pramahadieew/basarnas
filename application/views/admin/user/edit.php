<div class="col-md-12">
  <?php

echo validation_errors('<div class="alert alert-warning">', '</div>');

//error upload file
if (isset($error)) {
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

echo form_open_multipart(base_url('admin/User/edit/' . $item[0]->nama_id));
?>
  <?php foreach ($item as $user) {?>

  <div class="form-group">
    <label>Name User</label>
    <input type="text" name="nama" class="form-control" value="<?php echo $user->nama; ?>" placeholder='Nama User'
      required>
  </div>
</div>

<div class="col-md-12">
  <div class="form-group">
    <label>Username</label>
    <input type="text" name="username" class="form-control" value="<?php echo $user->username; ?>"
      placeholder='Masukan Username' required>
  </div>
</div>

<div class="col-md-12">
  <div class="form-group">
    <label>Email</label>
    <input type="text" name="email" class="form-control" value="<?php echo $user->email; ?>" placeholder='Masukan Email'
      required>
  </div>
</div>

<div class="col-md-12">
  <div class="form-group">
    <label>Password User</label>
    <input type="password" name="password" class="form-control" value="" placeholder='' >
  </div>
</div>

<!-- <div class="col-md-12">
  <div class="form-group">
    <label>Foto User</label>
    <input type="file" name="gambar" class="form-control">
  </div>
</div> -->
<?php ;}?>

<div class="col-md-12">
<div class="form-group">
  <input type="submit" name="submit" class="btn btn-success" value="Simpan Data">
  <input type="reset" name="reset" class="btn btn-default" value="Reset">
</div>
</div>

<?php echo form_close(); ?>