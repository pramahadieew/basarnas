<form action="<?=site_url('admin/scan')?>" class="" method="get">
	<div class="row">
		<div class="col-md-2">
		
			<i class="fa fa-search"></i>
			<label>Scan Result</label>
			<input type="text" name="scan" id="scan" placeholder="Kode Barang - Nup" class="form-control" autocomplete="off" autofocus/>
        </div>
        <div class="col-md-2">
			<br />
			<input type="hidden" id="" class="btn btn-success"/>
		</div>
	</div>	
</form>
<br />

    <div class="row" id="result">
	<div class="col-md-6">
		<?php echo $this->session->flashdata('notif') ?>
		<?php if($items) { foreach ($items as $scan) { ?>
		<!-- USERS LIST -->
		<div class="box box-yellow">
			<div class="box-header with-border">
				<h3 class="box-title">List</h3>

				<div class="box-tools pull-right">
					<!-- <span class="label label-danger">8 New Members</span> -->
					<!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button> -->
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<ul class="users-list clearfix">
					<div class="col-md-12">
						<div class="col-md-4">
							<div class="form-group">
								<label>Kode Barang</label>
								<div><?php echo $scan->kd_brg ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>No Aset</label>
								<div><?php echo $scan->no_aset ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Kode Lokasi</label>
								<div><?php echo $scan->kd_lokasi ?></div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="col-md-4">
							<div class="form-group">
								<label>Nama Barang</label>
								<div><?php echo $scan->ur_baru ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Tanggal perolehan</label>
								<div><?php echo $scan->tgl_perlh ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Merek/Type</label>
								<div><?php echo $scan->merk_type ?></div>
							</div>
						</div>	
					</div>
					
					<div class="col-md-12">	
						<div class="col-md-4">
							<div class="form-group">
								<label>Asal</label>
								<div><?php echo $scan->asal_perlh ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Rph Satuan</label>
								<div><?php echo $scan->rph_sat ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Keterangan</label>
								<div><?php echo $scan->keterangan ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="col-md-4">
							<div class="form-group">
								<label>No. Mesin</label>
								<div><?php echo $scan->no_mesin ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>No. Rangka</label>
								<div><?php echo $scan->no_rangka ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>No. Polisi</label>
								<div><?php echo $scan->no_polisi ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">		
						<div class="col-md-4">
							<div class="form-group">
								<label>No. BPKB</label>
								<div><?php echo $scan->no_bpkb ?></div>
							</div>
						</div>
					</div>
				</ul>
			</div>
			<!-- /.box-body -->
			<div class="box-footer text-center">
				<!-- <a href="javascript:void(0)" class="uppercase">View All Users</a> -->
			</div>
			<!-- /.box-footer -->
		</div>
		<!--/.box -->
		<?php } } ?>
        </div>
	<!-- /.col -->
</div>
<!-- /.row -->