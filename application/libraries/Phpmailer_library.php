<?php
class phpmailer_library
{
    protected $objMail;
    public function __construct()
    {
        log_message('Debug', 'PHPMailer class is loaded.');
        require_once APPPATH . 'third_party/phpmailer/src/PHPMailer.php';
        require_once APPPATH . 'third_party/phpmailer/src/SMTP.php';

        $this->objMail            = new PHPMailer\PHPMailer\PHPMailer();
        $this->objMail->SMTPDebug = PHPMailer\PHPMailer\SMTP::DEBUG_SERVER; // Enable verbose debug output
        $this->objMail->isSMTP(); // Send using SMTP
        $this->objMail->Host       = 'srv66.niagahoster.com'; // Set the SMTP server to send through
        $this->objMail->SMTPAuth   = true; // Enable SMTP authentication
        $this->objMail->Username   = 'sales@putramandirisukses.com'; // SMTP username
        $this->objMail->Password   = 'admin123'; // SMTP password
        $this->objMail->SMTPSecure = PHPMailer\PHPMailer\PHPMailer::ENCRYPTION_STARTTLS; // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
        $this->objMail->Port       = 587; // TCP port to connect to
    }

    public function sendEmailPenawaran(array $data)
    {

//Recipients
        $this->objMail->setFrom('sales@putramandirisukses.com', 'Sales');
        $this->objMail->addAddress($data['email']); // Add a recipient
        // $this->objMail->addReplyTo('info@example.com', 'Information');
        // $this->objMail->addCC('');
        // $this->objMail->addBCC('');

// Content
        $this->objMail->isHTML(true); // Set email format to HTML
        $this->objMail->Subject = $data['subyek'];
        $this->objMail->Body    = $data['body'];
        $this->objMail->AltBody = $data['body'];

        $this->objMail->send();
        return 'Message has been sent';

    }
}
